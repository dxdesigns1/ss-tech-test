<?php

namespace Tests;

use Entities\Facility;
use PHPUnit\Framework\TestCase;

class FacilityTest extends TestCase
{
    protected $facility;

    /**
     * Call this template method before each test method is run.
     */
    protected function setUp(): void
    {
        $this->facility = new Facility;
    }

    /**
     * Based on the state suffix, check if it matches the county name.
     */
    public function test_get_formatted_county()
    {
        $this->facility->setState('AK');
        $this->facility->setCounty('Ashley');
        $this->assertEquals($this->facility->getFormattedCounty(), 'Ashley Borough');
    }
}
