<?php
    //////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Create a function that accepts an array, and returns the array in reverse without using 
     * array.reverse (JavaScript) or array_reverse (PHP).
     */
    $source = array('Apple', 'Banana', 'Orange', 'Coconut');
    $size = sizeof($source);

    $return = array();
    for($i=$size-1; $i >= 0; $i--){
        array_push($return, $source[$i]);
    }

    echo "Results: " . implode(', ', $return);

    //////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Create a function that determines whether a string is a palindrome.
     */
    function Palindrome($string){
        // Get the original string value.
        $original = $string;

        // Remove spaces and case-sensitivity.
        $string = str_replace(' ', '', strtolower($string));

        $l = 0;
        $r = strlen($string) - 1;
        $flag = 0;

        while($r > $l){
            if($string[$l] != $string[$r]){
                $flag = 1;
                break;
            }

            $l++;
            $r--;
        }

        if($flag == 0){
            echo "<b>$original</b> is a Palindrome string.";
        }else{
            echo "<b>$original</b>" . " is NOT a Palindrome string.";
        }
    }

    // Results.
    echo Palindrome("level") . '<br>';          // level is a Palindrome string.
    echo Palindrome("levels") . '<br>';         // levels is NOT a Palindrome string.
    echo Palindrome("Yo banana boy") . '<br>';  // Yo banana boy is a Palindrome string.
?>
