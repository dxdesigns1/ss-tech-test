<?php

namespace App\Repositories;

use App\Models\Payment;

class PaymentRepository
{
    /**
     * QUESTION #4: REFACTOR
     * > Get all payments, with the user, ordered by id descending, limited to 20 results
     * 
     * @param array reqOptions 
     * 
     * @return ?array An array of payments.
     */
    public function getAll(array $reqOptions): ?array
    {
        // Table with roughly 1.2m records
        $payments = Payment::limit($reqOptions['limit'] ?? 20)->with('user', 'facility');

        if ($payments->count() === 0) {
            return $return;
        }

        if ($reqOptions['sortDir'] === 'desc') {
            $payments = $payments->orderBy('id', 'desc');
        }

        return $payments->get()->toArray();
    }     
}
